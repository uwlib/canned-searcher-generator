# Canned Search Generator

This code is a web page for generating links to search queries in [UW Libraries Search](http://search.lib.uw.edu). It is specified specifically to UW and includes our search scopes and sorting options. Note that if we change these within Primo, we must update the code.

The overall page is a fork of [Jim Robinson's Primo Search Generator](https://github.com/Jim-Robinson/primo) on GitHub. Our instance hides much of the interface and only generates search links. The ability to generate a search form is disabled.